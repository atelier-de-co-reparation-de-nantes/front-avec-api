$(document).ready(function(){

    // Constructing the suggestion engine
    var objets = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: ['vélo','voiture télécomander','bouilloir','grille pain','ordinateur','pull']
    });
    objets.initialize();
    
    // Initializing the typeahead with remote dataset
    $('.autocompletion-objet').typeahead(null, {
        hint: true,
        highlight: true,
        name: 'objets',
        source: objets,
        limit: 10 
    });
});  