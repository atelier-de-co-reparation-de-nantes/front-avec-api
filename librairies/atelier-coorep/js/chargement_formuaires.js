//chargement formulaire
$('#boutton--nouveau-elle--utilisateur-ise').click(function () {
  $('#nouvelle__utilisateur td').load('formulaire/utilisateur.html');
});
var liste_objet_panne;
$.getJSON("jsonTest/liste_pannes.json", function( data ) {liste_objet_panne=data;});

//chargement tableau
$(document).ready(function () {
  $.getJSON("jsonTest/liste_utilisateur.json", function (data) {
    $.each(data, function ( ) {
      $('.tableau__utilisateur>.table>tbody').append(
              '<tr id="utilisateur__' + this.id + '">' +
              '        <td>' + this.nom + '</td>' +
              '        <td>' + this.mail + '</td>' +
              '        <td class="text-right">' +
              '            <button type="submit" class="btn btn-info edit__objets" data-toggle="collapse"  data-target="#editer__utilisateur__objets__' + this.id + '" aria-controls="editer__objet__' + this.id + '">' +
              '                Voir les objets <span class="glyphicon glyphicon-barcode" aria-hidden="true"></span>' +
              '            </button>' +
              '            <button type="submit" class="btn btn-info edit__utilisateur" data-toggle="collapse"  data-target="#editer__utilisateur__' + this.id + '" aria-controls="editer__utilisateur__' + this.id + '">' +
              '                Editer <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>' +
              '            </button>' +
              '        </td>' +
              '    </tr>' +
              '    <tr class="collapse" id="editer__utilisateur__' + this.id + '">' +
              '        <td class="well" colspan="3">' +
              '        </td>' +
              '    </tr>' +
              '    <tr class="collapse" id="editer__utilisateur__objets__' + this.id + '">' +
              '    <td class="well" colspan="3">' +
              '        <p class="text-right">' +
              '            <button class="btn btn-primary" type="button"  data-toggle="collapse"  data-target="#nouvelle__objet" aria-controls="nouvelle__objet">' +
              '                Ajouter un objet  <span class="glyphicon glyphicon-barcode" aria-hidden="true"></span>' +
              '            </button>' +
              '        </p>' +
              '        <table class="table table-striped table-bordered">' +
              '            <tr>' +
              '                <th class="col-md-5">Objet</th>' +
              '                <th class="col-md-5">pane</th>' +
              '                <th class="col-md-2">Action</th>' +
              '           </tr>' +
              '        </table>' +
              '    </td>' +
              '</tr>'
              );
      var value = this;
      $("#utilisateur__" + this.id + " .edit__utilisateur").click(value, function () {
        $("#editer__utilisateur__" + value.id + " td").load('formulaire/utilisateur.html', function () {
          $("#editer__utilisateur__" + value.id + " .InputPseudo").attr("value", value.nom);
          $("#editer__utilisateur__" + value.id + " .InputEmail").attr("value", value.mail);
        });
      });
      $("#utilisateur__" + this.id + " .edit__objets").click(value, function () {
      $('#editer__utilisateur__objets__'+ value.id +' table tbody').html(
              '            <tr>' +
              '                <th class="col-md-5">Objet</th>' +
              '                <th class="col-md-5">pane</th>' +
              '                <th class="col-md-2">Action</th>' +
              '           </tr>');
        $.getJSON("jsonTest/objets_utilisateur_" + value.id + ".json", function (data_user) {
          $.each(data_user, function ( ) {
            $('#editer__utilisateur__objets__'+ value.id +' table tbody').append(
                    '      <tr>'+
                    '          <td>'+liste_objet_panne[this.id_objet].objet+'</td>'+
                    '          <td>'+liste_objet_panne[this.id_objet].panne[this.id_panne].nom+'</td>'+
                    '          <td class="text-right">'+
                    '              <button type="submit" class="btn btn-info" data-toggle="collapse"  data-target="#editer__objet__'+this.id_objet+'" aria-controls="editer__objet__'+this.id_objet+'">'+
                    '                  Editer <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>'+
                    '              </button>'+
                    '          </td>'+
                    '      </tr>'
            );
          });
        });
      });
    });
  });
});